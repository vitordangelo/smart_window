openPin = 4
closePin = 0

gpio.mode(openPin, gpio.OUTPUT)
gpio.mode(closePin, gpio.OUTPUT)

tmr.alarm(0, 2000, 1, function()
  gpio.write(openPin, 0)
  gpio.write(closePin, 1)
end)

tmr.alarm(1, 4000, 1, function()
  gpio.write(openPin, 1)
  gpio.write(closePin, 0)
end)