config = require("config")

local setup = {}

function setup.setupPins()
  gpio.mode(config.openedWindowSensor, gpio.INPUT, gpio.PULLUP)
  gpio.mode(config.closedWindowSensor, gpio.INPUT, gpio.PULLUP)
  gpio.mode(config.openWindow, gpio.OUTPUT)
  gpio.mode(config.closeWindow, gpio.OUTPUT)
  gpio.mode(config.led, gpio.OUTPUT)
end

function setup.blink()
  ledState = 0
  tmr.alarm(0, 400, 1, function()
    ledState = 1 - ledState;
    gpio.write(config.led, ledState)
  end)
end

function setup.openWindow()
  gpio.write(config.closeWindow, 0)
  gpio.write(config.led, 0)
end

function setup.closeWindow()
  gpio.write(config.openWindow, 0)
  gpio.write(config.closeWindow, 1)
end

function setup.breakWindow()
  gpio.write(config.openWindow, 0)
  gpio.write(config.closeWindow, 0)
end

function setup.statusWindow()
  tmr.alarm(1, 400, 1, function()
    local sensorClosed = gpio.read(config.closedWindowSensor)
    local sensorOpened = gpio.read(config.openedWindowSensor)
    if (sensorClosed == 0) then
      print("Window Closed")
      setup.breakWindow()
    end
    if (sensorOpened == 0) then
      print("Window Opened")
      setup.breakWindow()
    end
  end)
end

return setup